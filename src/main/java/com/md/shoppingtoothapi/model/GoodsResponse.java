package com.md.shoppingtoothapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class GoodsResponse {
    private Long id;
    private String thumbImg;
    private String name;
    private Double price;
    private LocalDateTime dateCreate;
}
