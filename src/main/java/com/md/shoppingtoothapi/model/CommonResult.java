package com.md.shoppingtoothapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private String msg;
    private Integer code;
}
