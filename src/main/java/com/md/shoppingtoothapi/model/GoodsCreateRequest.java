package com.md.shoppingtoothapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class GoodsCreateRequest {
    private String thumbImg;
    private String name;
    private Double price;
}
