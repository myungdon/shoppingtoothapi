package com.md.shoppingtoothapi.repository;

import com.md.shoppingtoothapi.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
    Object findAllById(long id);
}
