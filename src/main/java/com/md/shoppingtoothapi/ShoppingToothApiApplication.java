package com.md.shoppingtoothapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingToothApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingToothApiApplication.class, args);
    }

}
