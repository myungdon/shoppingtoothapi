package com.md.shoppingtoothapi.controller;

import com.md.shoppingtoothapi.model.*;
import com.md.shoppingtoothapi.service.GoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @PostMapping("/new")
    public CommonResult setGoods(@RequestBody GoodsCreateRequest request){
        goodsService.setGoods(request);

        CommonResult response = new CommonResult();
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/all")
    public ListResult<GoodsItem> getList(){
        List<GoodsItem> list = goodsService.getGoodss();

        ListResult<GoodsItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/detail/{id}")
    public SingleResult<GoodsResponse> getGoods(@PathVariable long id){
        GoodsResponse result = goodsService.getGoods(id);

        SingleResult<GoodsResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다.");
        response.setCode(0);
        response.setData(result);

        return response;
    }
}
