package com.md.shoppingtoothapi.service;

import com.md.shoppingtoothapi.entity.Goods;
import com.md.shoppingtoothapi.model.GoodsCreateRequest;
import com.md.shoppingtoothapi.model.GoodsItem;
import com.md.shoppingtoothapi.model.GoodsResponse;
import com.md.shoppingtoothapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public void setGoods(GoodsCreateRequest request){
        Goods addData = new Goods();
        addData.setThumbImg(request.getThumbImg());
        addData.setName(request.getName());
        addData.setPrice(request.getPrice());
        addData.setDateCreate(LocalDateTime.now());

        goodsRepository.save(addData);
    }

    public List<GoodsItem> getGoodss(){
        List<Goods> originList = goodsRepository.findAll();

        List<GoodsItem> result = new LinkedList<>();

        for (Goods goods : originList){
            GoodsItem addItem = new GoodsItem();
            addItem.setId(goods.getId());
            addItem.setThumbImg(goods.getThumbImg());
            addItem.setName(goods.getName());
            addItem.setPrice(goods.getPrice());
            addItem.setDateCreate(goods.getDateCreate());
            result.add(addItem);
        }

        return result;
    }

    public GoodsResponse getGoods(long id){
        Goods goods = goodsRepository.findById(id).orElseThrow();

        GoodsResponse response = new GoodsResponse();
        response.setId(goods.getId());
        response.setThumbImg(goods.getThumbImg());
        response.setName(goods.getName());
        response.setPrice(goods.getPrice());
        response.setDateCreate(goods.getDateCreate());

        return response;
    }
}
